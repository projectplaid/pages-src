# Welcome to Project Plaid

Project Plaid is the umbrella name for the personal projects of [Robert Roland](mailto:rob@projectplaid.com)

## Active Projects

* [aseL4](https://codeberg.org/projectplaid/aseL4) - a set of Ada bindings to the seL4 kernel
* [plaidst](https://codeberg.org/projectplaid/plaidst) - a Smalltalk environment built in Ada
